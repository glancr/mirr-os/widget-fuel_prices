# FuelPrices
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'fuel_prices'
```

And then execute:
```bash
$ bundle
```

## More information
Visit [glancr.de](https://glancr.de) for more information.

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
