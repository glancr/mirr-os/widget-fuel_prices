# frozen_string_literal: true

module FuelPrices
  class Configuration < WidgetInstanceConfiguration
    attribute :api_key, :string, default: ""
    attribute :lat, :string, default: ""
    attribute :lng, :string, default: ""
    attribute :radius, :integer, default: 5
    attribute :type, :string, default: "e5"
    attribute :sort, :string, default: "price"
    attribute :show_last_refresh, :boolean, default: false

    validates :type, inclusion: %w(e5 e10 diesel)
    validates :sort, inclusion: %w(price dist)
    validates :radius,
              numericality: {
                only_integer: true,
                greater_than: 0,
                less_than_or_equal_to: 25
              }

    # Tankerkönig API keys are in UUID format.
    validates :api_key,
              presence: true,
              format: /\A[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}\z/,
              allow_blank: true
    validates :lng,
              presence: true,
              format: { with: /\A-?(([-+]?)(\d{1,3})((\.)(\d+))?)\z/ },
              allow_blank: true

    validates :lat,
              presence: true,
              format: { with: /\A-?([1-8]?[1-9]|[1-9]0)\.\d{1,15}\z/ },
              allow_blank: true

    # TODO: Add API key validation and after_validation callback to replace frontend logic.
  end
end
