module FuelPrices
  class Engine < ::Rails::Engine
    isolate_namespace FuelPrices
    config.generators.api_only = true
  end
end
